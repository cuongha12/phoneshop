import './App.css';
import Routes from "./routes";
import Main from "./components/Main";
import "antd/dist/antd.css";

function App() {
	return (
		<Main>
			<Routes/>
		</Main>
	);
}

export default App;
