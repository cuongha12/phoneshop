const path = {
	HOME: '/',
	BLOG: '/blog',
	SHOP: '/shop',
	CONTACT: '/contact',
}

export default path